<?php
include dirname(__DIR__,2).'/src/EnvHelper.php';

class EnvHelperTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testInit() {
        $envFile = dirname(__DIR__).'/_data/.env';
        putenv("ENV_FILE=".$envFile);
        $this->assertTrue(\uhi67\envhelper\EnvHelper::init());
        $expected = [
            'var1' => 'testvalue1',
            'var' => [
                'false' => false,
                'null' => null,
                'true' => true,
                'text' => 'false',
                'empty' => '',
            ],
            'other' => [
                'newNull' => null,
                'new2' => null
            ],
        ];
        $default = [
            'var1' => null,
            'var' => [
                'false' => 1,
                'null' => 2,
                'true' => false,
                'text' => false,
                'empty' => false,
            ],
            'other' => [
                'newNull' => 'EnvHelper::NULL',
                'new2' => null
            ],
        ];
        foreach($default as $var=>$defaultValue) {
            $value = \uhi67\envhelper\EnvHelper::getEnv($var, $defaultValue);
            $this->assertSame($expected[$var], $value);
        }
    }

    public function testMissing() {
        $envFile = dirname(__DIR__) . '/_data/.env';
        putenv("ENV_FILE=" . $envFile);
        $this->assertTrue(\uhi67\envhelper\EnvHelper::init());
        $this->expectException(Exception::class);
        $value = \uhi67\envhelper\EnvHelper::getEnv('missing', false);
    }

    public function testMissing2() {
        $envFile = dirname(__DIR__) . '/_data/.env';
        putenv("ENV_FILE=" . $envFile);
        $this->assertTrue(\uhi67\envhelper\EnvHelper::init());
        $this->expectException(Exception::class);
        $value = \uhi67\envhelper\EnvHelper::getEnv('missing', \uhi67\envhelper\EnvHelper::UNDEFINED);
    }

    public function testQuoted() {
        $envFile = dirname(__DIR__) . '/_data/.env';
        putenv("ENV_FILE=" . $envFile);
        $this->assertTrue(\uhi67\envhelper\EnvHelper::init());
        $this->assertSame('value quoted', \uhi67\envhelper\EnvHelper::getEnv('varQuoted'), false);
        $this->assertSame('value with inner " and \\', \uhi67\envhelper\EnvHelper::getEnv('varInner'), false);
    }
}
