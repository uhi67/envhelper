<?php

namespace uhi67\envhelper;

use Exception;

class EnvHelper {
    static $init;
    const UNDEFINED = 'EnvHelper::UNDEFINED';
    const FALSE = 'EnvHelper::FALSE';
    const TRUE = 'EnvHelper::TRUE';
    const NULL = 'EnvHelper::NULL';
    static $secretDir = null;

    /**
     * Reads file containing environment variables.
     * Populates content into the environment.
     * Specified file must exist and contain lines like this:
     *
     * `VAR= "value"`
     *
     * Spaces around = and double quotes are optional. Space in value allowed only if value is quoted. # denotes comment
     * Space before = is not recommended (compatibility with docker .env format)
     *
     * @return bool -- success
     * @throws Exception
     */
    static public function init() {
        if(self::$init) return true;
        $file = getenv('ENV_FILE');
        if(!$file) {
            $file = '.env';
            $file = dirname(dirname(dirname(dirname(__DIR__)))) . '/' . $file;
        }
        if(!$f = @fopen($file, 'r')) {
            $var = 'ENV_ERROR';
            $value = "ENV file '$file' not found";
            putenv("$var=$value");
            $_ENV[$var] = $value;
            self::$init = true;
            return false;
        }
        $i = 0;
        while(!feof($f)) {
            $l = trim(fgets($f));
            $i++;
            // comment at the beginning of the line
            if(preg_match('~^\s*#~', $l)) continue;
            if($l == '') continue;
            $var = null;
            // quoted value with optional comment at the end of the line. The value may contain space and #. May contain double quotes escaped with \.
            if(preg_match('~^\s*(\w+)\s*=\s*"([^"]*(\"[^"]+)*)"\s*(#.*)?$~', $l, $mm)) {
                $var = $mm[1];
                $value = preg_replace('~\\\\(["\\\\])~', '$1', $mm[2]);
            } // unquoted value with optional comment at the end of the line. The value must not contain space, double quote or #.
            elseif(preg_match('~^\s*(\w+)\s*=\s*([^\s"]*)\s*(#.*)?$~', $l, $mm)) {
                $var = $mm[1];
                $value = $mm[2];
            } else {
                throw new Exception("Syntax error at line $i in `$file`\n");
            }
            if($var) {
                $_ENV[$var] = $value;
                if(function_exists('apache_setenv')) apache_setenv($var, $value);
                else putenv("$var=$value");
            }
        }
        self::$init = true;
        return true;
    }

    /**
     * Returns an environment variable or default or Exception
     * If `$default` is an array, this call will retrieve multiple variables, each for all keys in `$default` array.
     * If default is not specified (or false is given), the variable is mandatory: an exception will be thrown if not exists.
     *
     * Using false in default deprecated, use EnvHelper::UNDEFINED for mandatory values or EnvHelper::FALSE to be false default.
     *
     * Special values:
     *
     * EnvHelper::UNDEFINED -- as default means mandatory value in the environment (currently same as false)
     * EnvHelper::TRUE      -- converted to true. Usable in env as string, or in php as constant in default value.
     * EnvHelper::FALSE     -- converted to false. Usable in env as string, or in php as constant in default value.
     * EnvHelper::NULL      -- converted to null. Usable in env as string, or in php as constant in default value.
     *
     * @param string $var
     * @param string|array|boolean $default - false (default) for mandatory, null for empty default.
     * @return string|array
     * @throws Exception
     */
    public static function getEnv($var, $default = EnvHelper::UNDEFINED) {
        if(is_array($default)) {
            array_walk($default, function(&$value, $key) use ($var) {
                if(!is_integer($key)) $value = static::getEnv($var . '_' . $key, $value);
            });
            return $default;
        }
        if(!self::$init) static::init();
        $var = strtoupper(static::underscore($var));

        if(isset($_ENV[$var])) return static::convert($_ENV[$var]);
        else {
			$value = getenv($var, true);
            if($value === false) $value = getenv($var);
            if($value === false && ($default === false || $default === EnvHelper::UNDEFINED)) {
                throw new Exception("Configuration error: Environment variable '$var' is missing");
            }
        }
        return static::convert($value === false ? $default : $value);
    }

    /**
     * @param string $value
     *
     * @return bool|string|null
     */
    public static function convert($value) {
        if($value === EnvHelper::TRUE) return true;
        if($value === EnvHelper::FALSE) return false;
        if($value === EnvHelper::NULL) return null;
        return $value;
    }

    /**
     * Converts any "CamelCased" into an "underscored_word".
     * Does not convert uppercase-only words.
     *
     * @param string $words the word(s) to underscore
     * @return string
     */
    public static function underscore($words) {
        if(!preg_match('~[a-z]~', $words)) return $words;
        return mb_strtolower(preg_replace('/(?<=\\pL)(\\p{Lu})/u', '_\\1', $words), 'UTF-8');
    }
    /**
     * Returns a value from secrets or default.
     *
     * If the file at $SECRET_DIR/name exists, returns  the contents of the file, otherwise returns the default value.
     *
     * @param string $name
     * @param string $default
     * @return void
     */
    public static function getSecret($name, $default = self::UNDEFINED) {
        if(static::$secretDir===null) static::$secretDir = static::getEnv('SECRET_DIR', '/run/secrets');
        if(static::$secretDir=='') return self::getEnv($name, $default);
        if(substr(static::$secretDir,0,2)=='./') static::$secretDir = dirname(__DIR__,4).substr(static::$secretDir,1);
        if(file_exists(static::$secretDir.'/'.$name)) return rtrim(file_get_contents(static::$secretDir.'/'.$name));
        if($default == self::UNDEFINED)
            throw new Exception("Configuration error: Secret '$name' is missing in '".static::$secretDir."'");
        return $default;
    }

}
