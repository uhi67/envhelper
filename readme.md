EnvHelper
=========

version 2.4 -- 2023-12-20

If you want to make your application be independent of different environments of installations, you should put the 
varying part outside the application, for example, into environment variables.    
This is a simple utility for any application to retrieve values of different kinds of environments.

Environment variables may be defined in:

- apache configuration, in SetEnv clauses. This is appropriate for traditional virtual hosts in production environments;
- docker-compose.yml in docker environment (sometimes the only way to configure the installation),
- `/.env` file -- values defined here will overwrite the others if this module is used.

In addition for sensitive data, use getSecret() to retrieve passwords or other classified data from files in secret directory. 
The docker environment provides secret directory in a secure way, or you can emulate it yourself in any directory.     

Using EnvHelper is simple
-------------------------

### Installation

`composer require uhi67/envhelper dev-master`

### Case 0: single value without default

    EnvHelper::init(); // Call it once
    $value = getenv($var); // get value in usual way

### Case 1: single value with default

Example:

    $db_name = EnvHelper::getEnv('dbName', 'db1'); // No explicit EnvHelper::init() call is required
    
The name of variable will be converted to uppercase uncamelized, for example 'DB_NAME'.    

EnvHelper will search for value in the following order:
- `/.env` if the file exists
- external environment via standard getenv() function 
    
If the variable none of them is found in, and default value is supplied, that is returned.
If default value is null, null value is returned. 
If neither default value is defined (or default is `EnvHelper::UNDEFINED`), an exception is raised.     

### Case 2: array of values

Example:

    $db = EnvHelper::getEnv('db', [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=db1',
        'username' => 'db1user',
        'password' => null,
        'charset' => 'utf8',
    ]);
    
In this case, EnvHelper retrieves values independently for all elements of array, one-by-one just like in case 1.
Each item will be a single environment variable, named for example 'APP_DB_CLASS' or 'DB_CLASS' index in conf.php, 
and so on.

### Special values

Use EnvHelper::UNDEFINED as default if the value is mandatory int the environment. An exception will be raised if not exists.

Environment variables may have special string values `'EnvHelper::FALSE', 'EnvHelper::TRUE', 'EnvHelper::NULL'` to 
result `false`, `true`, `null` values. As default, you may use the constant syntax as well. 
Note: only `EnvHelper::getEnv()` can recognize these special values and return the non-string values. 

### Settings and other behavior

The alternative location of `.env` may be defined in `ENV_FILE` variable, relative to the root directory 
of the application. The content of `.env` is read once at initialization.

The `.env` file may be part of the project, saved into VCS, or used only locally for development if you don't want 
to restart apache after every change.   

`SECRET_DIR` variable holds the location of the secret values. Default value is `/run/secrets` which is the default in Docker environment.
Explicit defined empty value results reading secrets from environment variables.
Values starting with `./` means relative directory to the parent of the vendor dir (probably the application root). 

Security Warning
================
It’s a lot easier to leak an environment variable than it is to leak a PHP variable. 
For secrets use getSecret() instead of getEnv().

- never leave phpinfo() call in production environment: it shows $_ENV and $_SERVER
- Don't leave debugging features in production environment: debugger may log the content of $_ENV and $_SERVER
- Don't expose in any other ways $_ENV and $_SERVER
- Don't let the webserver to serve .env file

Consider disabling phpinfo function in php.ini.

Change log
==========

## 2.4 -- 2023-12-20

- getSecret fallback to env. var
- getSecret supports app-relative secret dir

## 2.3 -- 2023-12-15

- getSecret() added
- php5.6 compatibility restored
 
## 2.2.1 -- 2021-11-14

- Inner \" and \\ bug

## 2.2 -- 2021-10-11

- ENV_FILE varible if used, must contain full path;
- Special values (string and constant forms too)
   - 'EnvHelper::UNDEFINED' (use only as default)
   - 'EnvHelper::FALSE'
   - 'EnvHelper::TRUE';
   - 'EnvHelper::NULL';

## 2.1 -- 2021-03-10

- .env has priority
- Security warning
- getenv() is also effective after init

## 2.0 -- 2020-12-11

- using /.env file instead of /config/env.php
- loads into env, can be read by getenv() as well
- APP_ prefix is not prepended automatically
- custom env file name is defined in "ENV_FILE" variable

## 1.0 -- 2019-04-10

- first release uses /config/env.php and does not load into real env.
